package in.connect2tech.rest.session4;

/**
 * @author : Naresh Chaurasia
 * @email : message4naresh@gmail.com
 * @Code : https://bitbucket.org/connect2tech
 * @WebSite : https://connect2tech.in/ 
 * @Profile : https://goo.gl/2mCt6v
 * @YouTubeChannel: https://goo.gl/c7FAsq
 */

public class JsonToObjectFail {
	public String FaultId;
	public String fault;
	
	public String getFaultId() {
		return FaultId;
	}
	public void setFaultId(String faultId) {
		FaultId = faultId;
	}
	public String getFault() {
		return fault;
	}
	public void setFault(String fault) {
		this.fault = fault;
	}
	
	
	
}
