package in.connect2tech.rest.session4;

import org.json.simple.JSONObject;
import org.testng.annotations.Test;

import groovy.json.internal.JsonStringDecoder;
import io.restassured.RestAssured;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;

/**
 * @author : Naresh Chaurasia
 * @email : message4naresh@gmail.com
 * @Code : https://bitbucket.org/connect2tech
 * @WebSite : https://connect2tech.in/
 * @Profile : https://goo.gl/2mCt6v
 * @YouTubeChannel: https://goo.gl/c7FAsq
 */

public class JsonDeSerialization {

	@Test
	public void registration() {
		RestAssured.baseURI = "http://restapi.demoqa.com/customer";
		RequestSpecification request = RestAssured.given();

		JSONObject requestParams = new JSONObject();
		requestParams.put("FirstName", "Virender");
		requestParams.put("LastName", "Singh");
		requestParams.put("UserName", "sdimpleuser2dd2011");
		requestParams.put("Password1", "password1");
		requestParams.put("Email", "sample2ee26d9@gmail.com");

		request.body(requestParams.toJSONString());

		Response response = request.post("/register");

		JsonToObj jsonToObj = response.getBody().as(JsonToObj.class);
		System.out.println(jsonToObj.getFault());
	}

}
