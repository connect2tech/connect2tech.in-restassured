package in.connect2tech.rest.session2;

import org.testng.annotations.Test;

import io.restassured.RestAssured;
import io.restassured.http.Headers;
import io.restassured.http.Header;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;

/**
 * @author : Naresh Chaurasia
 * @email : message4naresh@gmail.com
 * @Code : https://bitbucket.org/connect2tech
 * @WebSite : https://connect2tech.in/
 * @Profile : https://goo.gl/2mCt6v
 * @YouTubeChannel: https://goo.gl/c7FAsq
 */

public class Header2_Session {
	
	@Test
	public void IteratingOverHeaders() {
		RestAssured.baseURI = "http://restapi.demoqa.com/utilities/weather/city";
		RequestSpecification httpRequest = RestAssured.given();
		Response response = httpRequest.get("/pune");

		// Get all the headers. Return value is of type Headers.
		// Headers class implements Iterable interface, hence we
		// can apply an advance for loop to go through all Headers
		// as shown in the code below
		/*String content_Len = response.header("Content-Length");
		System.out.println("content_Len="+content_Len);*/
		
		Headers headers = response.headers();
		
		for(Header header : headers){
			String name = header.getName();
			String value = header.getValue();
			
			System.out.println("name="+name);
			System.out.println("value="+value);
			System.out.println("----------------------------");
		}
		
	}
}
